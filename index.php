<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}

$result;

try{

    $errors = FALSE;
    if (empty($_POST['first-name'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['field-email'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['BIO'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['radio-sex'])) {
        print('Заполните пол.<br/>');
        $errors = TRUE;
    }
    if(($_POST['radio-sex']== "M" && $_POST['radio-sex']!= "W" ) || ($_POST['radio-sex']!= "M" && $_POST['radio-sex']== "W")) {
    $errors=FALSE;
    }else         {
        print('Некорректный пол.<br/>');
    $errors = TRUE;
    }
    if (empty($_POST['ch'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }
    $superp = ['1', '2', '3'];
    if (empty($_POST['superpower'])) {
        print('Выберите способность<br>');
        $errors = TRUE;
    }
    else {
        $super = $_POST['superpower'];
        foreach ($super as $value) {
            if (!in_array($value, $superp)) {
                print('Недопустимая способность<br>');
                $errors = TRUE;
            }
        }
    }
    if ($errors) {
        exit();
    }


    $sup= implode(",",$_POST['superpower']);

    $conn = new PDO("mysql:host=localhost;dbname=u40065", 'u40065', '4097591', array(PDO::ATTR_PERSISTENT => true));

    
    $user = $conn->prepare("INSERT INTO form SET name = ?, email = ?, dob = ?, sex = ?, libs = ?, bio = ?, che = ?");
    $user -> execute([$_POST['first-name'], $_POST['field-email'], date('Y-m-d', strtotime($_POST['field-date'])), $_POST['radio-sex'], $_POST['radio-limb'], $_POST['BIO'], $_POST['ch']]);
    $id_user = $conn->lastInsertId();
    foreach ($_POST['superpower'] as $value){
        $user1 = $conn->prepare("INSERT INTO super SET id_form = ?, super_name = ?");
        $user1->execute([$id_user, $value]);
        $result = true;
    }
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}


if ($result) {
  echo "Информация занесена в базу данных под ID №" . $id_user;
}
?>
